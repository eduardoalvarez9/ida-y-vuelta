import { Component, OnInit } from '@angular/core';
import { PlacesService } from 'src/app/services/places.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  myInterval = 1500;
  activeSlideIndex = 0;
  slides: {id: number, name:string ,image: string; text?: string}[];

  constructor(private placesService: PlacesService) { 
    this.slides = this.placesService.places;
  }
  
  ngOnInit(): void {
  }

}
