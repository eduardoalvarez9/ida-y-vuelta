import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PlacesService } from 'src/app/services/places.service';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {

  results: any = [];
  constructor(private route: ActivatedRoute, private placesServices: PlacesService) {

    this.route.params.subscribe(params => {
      this.results = history.state.data || [];
    });
   }

  ngOnInit(): void {
  }


}
