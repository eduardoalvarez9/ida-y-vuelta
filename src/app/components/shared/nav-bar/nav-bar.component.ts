import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { PlacesService } from 'src/app/services/places.service';

@Component({
  selector: 'nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  textFilter = '';
  searchIcon = faSearch;
  constructor(private router: Router, private placesService: PlacesService) { }

  ngOnInit(): void {
  }

  search(){
    let results = this.placesService.places.filter(s => s.name.toLowerCase().includes(this.textFilter.toLowerCase()));
    this.router.navigate(['/search/' + this.textFilter], {state: {data:results}});
  }

  onKeydown(event:any) {
    if (event.key === "Enter") {
      this.search();
    }
  }
}
