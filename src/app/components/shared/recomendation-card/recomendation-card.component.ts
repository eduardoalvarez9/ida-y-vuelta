import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'recomendation-card',
  templateUrl: './recomendation-card.component.html',
  styleUrls: ['./recomendation-card.component.css']
})
export class RecomendationCardComponent implements OnInit {

  @Input() places: Array<{id?:number, name:string ,image?: string, title?: string}> = [];
  constructor() { }

  ngOnInit(): void {
  }

}
