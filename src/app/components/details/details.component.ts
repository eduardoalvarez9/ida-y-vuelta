import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PlacesService } from 'src/app/services/places.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  id: string = '';
  place: {id?: number, name?: string, image?: string, price?: number, text?: string} = {};

  constructor(private route: ActivatedRoute, private placesServices: PlacesService) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.id = params['id'];
      this.place = this.placesServices.getPlaceById(parseInt(this.id)) || {};
    })
    
  }

}
