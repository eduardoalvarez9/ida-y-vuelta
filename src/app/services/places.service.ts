import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PlacesService {

  constructor() { }

  public places: {id: number, name: string, image: string; price?:number, text?: string, location?: string }[] = [
    {
      id:1, 
      name:'chinchayote', 
      image: 'assets/chinchayote.png',
      price: 1000,
      text: 'ipsum Lorem, ipsum dolor sit amet consectetur adipisicing elit. Eius, vel eaque officiis consequatur dolore provident molestiae, sequi iste vitae, commodi fuga! Ipsa ex, voluptates numquam et aliquam quasi quod eum.'},
    {
      id:2,
      name:'cuevas de taulabe',
      image: 'assets/taulabe.png', 
      price: 500,
      text:`Si está interesado en explorar cuevas en Honduras, las Cuevas de Taulabé definitivamente son un sitio para visitar. Es uno de los sistemas de cuevas más accesibles en el país, ubicado en el kilómetro 140 de la carretera que une San Pedro Sula con Tegucigalpa. A pesar de haber sido descubiertas desde 1969, hasta años recientes realmente se han acondicionado para recibir visitantes de forma segura. Las Cuevas de Taulabé son parte de un sistema de grutas naturales que se esparcen por todo el municipio de Taulabé. Como muchas cuevas, el interior de Taulabé es húmedo y caluroso. Gracias a los juegos de luces que se han instalado, el visitante goza de un escenario casi mágico, donde la admiración por la naturaleza se funde con un silencioso respeto.`},
    { 
      id:3, 
      name:'Lago de Yojoa', 
      image: 'assets/lago.png',
      price: 500,
      text:`Para los turistas, el lugar ofrece múltiples opciones para pasar varios días en la zona. En el Lago de Yojoa hay muchos restaurantes, también varios hoteles con habitaciones típicas del lugar, centros de recreación con piscinas, diversiones acuáticas, y zoológicos.
        Como el Lago de Yojoa está entre San Pedro Sula y otras ciudades grandes como Comayagua, Siguatepeque, Tegucigalpa, La Paz, y la zona sur de Honduras, muchos de los viajeros que transitan entre estas ciudades grandes aprovechan del Lago para hacer un alto en el camino, apreciarlo, así como disfrutar del pescado frito fresco en sus diversos tamaños y de diversas especies, entre otras comidas que ofrecen.
      La Zona en los alrededores del Lago tiene actividades turísticas de diferente interés como los Parques nacionales, Zoológico Joya Grande, Parque Eco-Arqueológico Los Naranjos, Ruinas del Castillo Bogran, Hidden River Hotel, D & D Brewey, Honduyate Marina, Cuevas de Taulabe, Cuevas "El Jute", Cataratas de Pulhapanzak, la Central Hidroeléctrica Francisco Morazan (conocida también como "El Cajón"), Mina "El Mochito", entre otras atracciones.`},
      {
        id:4,
        name: 'Los Ángeles Cabin',
        image: 'https://www.honduras.com/wp-content/uploads/2021/08/photo1629737549-768x573.jpeg',
        price: 1200,
        text: `Hermosas y acogedoras cabañas Los Ángeles Cabin, un nuevo destino del municipio Valle Ángeles en Francisco Morazán.
        Los Ángeles Cabin, es una nueva propuesta de cabañas, ideales para unas vacaciones en familia, con los amigos o tu pareja, y así aprovechar la hermosa vista que nos ofrece Valle de Ángeles.
        En estas cabañas podrás disfrutar de aire puro, paz y espacios al aire libre para despejar la mente desde estas cómodas cabañas en Valle de Ángeles.
        Como muchas otras cabañas de Honduras, fue inaugurada en el 2020 durante la pandemia, con el objetivo de ofrecerle a los turistas un espacio abierto para disfrutar de la naturaleza, lejos de la rutina de la ciudad.
        Lo mejor de estas cabañas es que son totalmente privadas ideales para disfrutar de espacios abiertos, montañas y naturaleza.
        ¿Qué ofrece la cabaña?
        Privacidad
        Sala amueblada
        Tv con cable
        Wi-Fi
        Cocina equipada
        2 Camas matrimoniales
        Baño completo
        Agua caliente
        Bañera de masajes bajo presión de agua
        Balcón en la habitación`
      },
      {
        id: 5,
        name: 'Cascada las camelias',
        image: 'https://www.hondurastips.hn/wp-content/uploads/2021/07/Cascada-5.jpg',
        price: 900,
        text: `La Cascada Las Camelias, Atlántida, es un lugar para disfrutar de agua natural y mucha diversión en una cascada.
        En este lugar vivirás una experiencia única, ya que conectaras con la naturaleza, conocerás lugares nuevos dentro de Honduras.
        La cascada esta rodeada de naturaleza, se encuentra en la falda de la montaña de Pico Bonito en Atlántida, no es un lugar muy conocido pero es muy fácil de llegar una vez estés en la montaña.
        Las Camelias, te dará una de las mejores experiencia en cascadas, ya que es un lugar virgen con frescas aguas, que invitan a dar un chapuzón.
        Es ideal si quieres vivir aventura con tu familia o amigos, es una profunda poza que te dejara impresionado con su belleza natural.`
      },
      {
        id: 6,
        name: 'Montaña Celaque',
        image: 'https://i0.wp.com/www.marcahonduras.hn/wp-content/uploads/2021/03/Celaque-1.jpg?resize=1024%2C640&ssl=1',
        price: 500,
        text: `Celaque  tiene un área total  de 26,378 hectáreas, de las cuales 14,521 hectáreas corresponden a la zona núcleo intocable y 11,857 hectáreas de la zona de amortiguamiento. su altura es de 2 mil 849 metros sobre el nivel del mar siendo la montaña más alta de Honduras
        El término lenca “Celaque” significa “recipiente de agua” y hace referencia, a los 11 ríos que fluyen de dicha montaña. Este es el mejor lugar conectar con la naturaleza y realizar senderismo.`
      },
      {
        id: 7,
        name: 'Cuevas de talgua',
        image: 'https://ihah.hn/wp-content/uploads/2019/03/cuevas-talgua-770x445.jpg',
        price: 300,
        text: 'Cuevas de Talgua es el nombre que recibe una cueva situada en el valle de Olancho, en el municipio de Catacamas, al noreste del país centroamericano de Honduras. Es a veces conocida como "La Cueva de las Calaveras brillantes" debido a la forma en que la luz se refleja de los depósitos de calcita que se encuentran en los restos óseos depositados allí.'
      }
  ];

  getPlaceById(id: number){
    return this.places.find(p => p.id === id);
  }


}
